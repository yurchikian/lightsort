class Catalog

  SETTINGS = YAML.load open 'settings.yml'

  def self.load
    @connection = Lrcat::Catalog.open SETTINGS['lrcat']
  end

  def self.unload
    Lrcat::Catalog.close
    @connection = nil
  end

  def self.load_random_image(n=1)
    self.load

    images = Lrcat::Catalog::Image.where(rating: nil).where.not(pick: -1.0).sample(n)
    image_objects = []
    threads = []

    images.each do |image|
      image_file = image.library_file
      folder = image_file.library_folder
      root_folder = folder.library_root_folder

      image_path = "#{root_folder.absolutePath}" +
                   "#{folder.pathFromRoot}" +
                   "#{image_file.originalFilename}"
      preview_path = "tmp/#{image.id_global}.jpg"

      threads << Thread.new(image_path, preview_path) do |ip, pp|
        MiniMagick::Tool::Convert.new do |convert|
          convert << ip
          convert.merge! ["-resize", "1024x10000"]
          convert << pp
        end
      end

      image_objects << { oringinal: image_file.originalFilename,
                       preview: preview_path,
                       rating: image.rating, pick: image.pick,
                       id_global: image.id_global }

    end

    threads.each(&:join)


    self.unload

    image_objects
  end

  def self.set_image_rating id_global, rating
    self.load

    image = Lrcat::Catalog::Image.where(id_global: id_global).first

    return unless image


    case rating
    when "1".."5"
      image.pick = 0 if image.pick == -1
      image.rating = rating.to_i
    when "reject"
      image.pick = -1
    when "pick"
      image.pick = 1
    end

    image.save

    self.unload
  end
end
