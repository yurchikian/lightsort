main =
  image_load: (n) ->

    n = 1 unless n

    loader = $("<div class=\"loader\"><i class=\"fa fa-spinner fa-spin\"></i> Loading... </div>")
    $(".content").append(loader)

    $.ajax {
      type: 'GET',
      url: "/picrandom/#{n}",
      success: (data) ->
        $(".content").append(data)
      complete: ->
        loader.remove()
    }

  init_events: ->
    $(".content").on 'click', '.action', ->
      btn = $(this)
      card = btn.closest(".card")
      id_global = card.data('id_global')
      rating = btn.data("rating")

      $.ajax {
        type: 'POST',
        url: "/rate/#{id_global}/#{rating}"
        success: ->
          card.css("opacity", 0.4)
      }

    $("#loadmore").click ->
      main.image_load(10)

    $(".effect-toggler").click ->
      toggle = $(this)
      content = $('.content')

      toggle.closest('.nav-item').toggleClass('active')
      content.toggleClass(toggle.data('effect'))


  init: ->
    main.image_load(10)
    main.init_events()


$(document).ready ->
  main.init()
