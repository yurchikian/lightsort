require 'bundler'
Bundler.require(:default, :web)

require './catalog.rb'

set :static, true
set :public_folder, './vendor/'

def view path, data = {}
  if File.exist? "src/#{path}.haml"
    Haml::Engine.new(File.read("src/#{path}.haml")).render(Object.new, data)
  else
    "Could not find haml file @ \"src/#{path}.haml\""
  end
end

# Main page
get '/' do
  view "main"
end

# Almost API
get '/picrandom/?:n?' do
  n = params[:n].to_i
  n = 1 if n == 0
  pictures = { pictures: Catalog.load_random_image(n) }
  view "pic", pictures
end

# Setting rating
post '/rate/:id_global/:rating' do
  Catalog.set_image_rating params[:id_global], params[:rating]
end


# Single Image
get '/tmp/:file.jpg' do
  filename = params[:file]

  if File.exist? "tmp/#{filename}.jpg"
    send_file "tmp/#{filename}.jpg"
  else
    puts "Image @ \"tmp/#{filename}.jpg\" could not be loaded."
  end

end

## Scripts & Styles
get '/js/:file.js' do
  filename = params[:file]

  if File.exist? "src/coffee/#{filename}.coffee"
    CoffeeScript.compile File.read "src/coffee/#{filename}.coffee"
  else
    """
      console.log(\"Script @ src/coffee/\\\"#{filename}.coffee\\\" could not be loaded\");
    """
  end
end


get '/styles/:file.css' do
  filename = params[:file]

  if File.exist? "src/sass/#{filename}.sass"
    Sass::Engine.new(File.read("src/sass/#{filename}.sass")).render
  else
    """
      body::after {
        content: 'Style @ \"src/sass/#{filename}.sass\" could not be loaded.';
      }
    """
  end
end
